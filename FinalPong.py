# ----------------------------------------------------
# File: FinalPong.py
# ----------------------------------------------------
# Author: Sayyeda Saadia Razvi, Sarah Elias, Jake Lewis, BitBucket handle (SaadiaR)
# ----------------------------------------------------
# Plaftorm: Windows 7
# Environment: Python 2.7 
# Libaries: MatPlotLib 1.3.1
#           Tkinter $Revision: 81008 $
#           Math
#           Random
#           Itertools
# ----------------------------------------------------
# Description: 
# This program creates a Pong Game using Model View Control 
# Model Class uses objects from Ball class, paddles class, monster, cloud, etc.
# Model class handles interaction/collision of ball with board and paddles, etc, and updates mathematical model
# View Class uses tkinter to independently create physical models in GUI 
# Controller class manages buttons for moving paddles
# ---------------------------------------------------


'''Model'''
from random import choice, randint
from math import sin, cos, pi, sqrt

#Global Variables
random_factor = [.2,.3,.45,.5,.65,.7,.8] # bunch of factors b/w 0 and 1
angle_list = [20,45,90,135,200,225,270,315] # bunch of angles around the circle
board_width = 1000
board_height = 650
time_interval = .01 # time per loop iteration

def dot(x, y): # 2D dot product
    return x[0]*y[0] + x[1]*y[1]

def scale(x, a): #2D scalar multiplication
    return (x[0]*a, x[1]*a)

def add(x, y): # 2D vector addition
    return (x[0]+y[0], x[1]+y[1])

class Ball():
    def __init__(self, xGravity, yGravity):
        # create ball with specified radius, color and other initial conditions
        self.name = "Ball"
        self.radius = 10
        self.color = "red"
        self.Initial_Conditions()
        self.Update_Position(xGravity, yGravity)
        self.dx = 0
        self.dy = 0

    def Initial_Conditions(self):
        # gives ball velocity with random angle
        # ensures reasonable angle chosen and ball never moves just vertically
        self.velocity = 500
        self.Random_Velocity_Angle([45,135,225,315])
        # centers ball on the board
        self.xPos = board_width/2
        self.yPos = board_height/2

    def Update_Position(self, xGravity, yGravity): 
        # uses the previous velocity to update the position. Affect of gravity also included.
        self.xPos += (self.xVelocity * time_interval + 0.5*xGravity*time_interval**2)
        self.yPos += (self.yVelocity * time_interval + 0.5*yGravity*time_interval**2)
        # updating the velocity. If gravity is zero, the velocity is unchanged
        self.xVelocity += xGravity * time_interval
        self.yVelocity += yGravity * time_interval     

    def Increase_Speed(self):
        # increases velocity of ball
        self.xVelocity *= 1.5
        self.yVelocity *= 1.5

    def Random_Velocity_Angle(self, list1 = angle_list): #random angle for velocity
        Angle = choice(list1)* pi/180
        self.xVelocity = self.velocity*sin(Angle)
        self.yVelocity = self.velocity*cos(Angle)
   
    def Reflect(self, surface):
        # uses a tuple for the normal vector of the surface the ball reflects off of
        velocity = (self.xVelocity,self.yVelocity)
        diagonal = -2 * dot(surface, velocity)
        if dot(surface, velocity) <= 0:
            velocity = add(velocity, scale(surface, diagonal))
            self.xVelocity = velocity[0]
            self.yVelocity = velocity[1]

    def Plane_Response(self, object1, plane, paddle1_width, paddle2_width):
        if plane == "horizontal": # hits top or bottom walls
            if object1.yPos < board_height/2: # in upper half, hits top wall
                object1.yPos = object1.radius 
                self.Reflect((0,1))
            else: # in lower half
                object1.yPos = board_height - object1.radius
                self.Reflect((0,-1))
        if plane == "vertical":
            if object1.xPos < board_width/2: # in left half
                object1.xPos = object1.radius + paddle1_width 
                self.Reflect((1,0))
            else: # in right half
                object1.xPos = board_width - object1.radius - paddle2_width 
                self.Reflect((-1,0))

    def Sphere_Response(self, sphere):
        # generic response for ball after collision with any spherical body
        # find distance b/w ball and object
        distance_vectorx = self.xPos - sphere.xPos
        distance_vectory = self.yPos - sphere.yPos
        norm_distance = sqrt((distance_vectorx)**2 + (distance_vectory)**2)
        if norm_distance == 0:
            norm_distance = 0.1
        # norm_distance = self.b1.radius + self.monster1.radius
        normalx = distance_vectorx/norm_distance
        normaly = distance_vectory/norm_distance
        norm_normal = sqrt(normalx**2 + normaly**2)
        # reset ball position
        self.xPos = sphere.xPos + normalx * (self.radius + sphere.radius)
        self.yPos = sphere.yPos + normaly * (self.radius + sphere.radius)
        # reset ball velocity
        self.Reflect((normalx, normaly))

    def Portal_Response(self, portal1_position, portal2_position):
        #changes position of ball to that of the other portal once it goes through
        if self.yVelocity < 0:
            self.yPos = board_height - self.radius - 1
            self.xPos = portal2_position
        elif self.yVelocity > 0:
            self.yPos = self.radius + 1
            self.xPos = portal1_position

class Monster(Ball):
    def __init__(self, name, radius, color, velocity):
        Ball.__init__(self, xGravity = 0, yGravity = 0)
        self.name = name
        self.radius = radius
        self.color = color
        self.xPos = choice(random_factor)*board_width
        self.yPos = choice(random_factor)*board_height
        self.velocity = velocity
        self.Random_Velocity_Angle()

class Paddle():
    def __init__(self, name, xPos, up, down):
        self.name = name
        self.length = 100
        self.radius = self.length/2 # for when shape changed to semi-circle
        self.width = 20
        self.color = 'blue'
        self.xPos = xPos
        self.Initialize_Paddle(up,down)
        self.dx = 0
        self.dy = 0

    def Initialize_Paddle(self, up, down):
        self.velocity = 8
        # reset key control
        self.Paddle_Direction(up, down)
        # centralize paddle
        self.yPos = board_height/2

    def Move(self, buttons):
        # makes up and down buttons for paddles so that pushing and releasing both change velocity
        # this way if you press both buttons you won't move
        in_up_range = (self.yPos - self.length/2 > 0)
        in_down_range = (self.yPos + self.length/2 < board_height)
        if (self.up_key in buttons and self.down_key not in buttons) and in_up_range:
            self.yPos -= self.velocity
        elif (self.down_key in buttons and self.up_key not in buttons) and in_down_range:
            self.yPos += self.velocity

    def Paddle_Direction(self, up, down): #determine which button moves up/down
        self.up_key, self.down_key = up, down

class Portal():
    def __init__(self, name, yPos):
        self.name = name
        self.width = 200
        self.yPos = yPos
        self.color = "DeepPink2"
        self.Move_Randomly()

    def Move_Randomly(self): #move to random position
        self.xPos = choice(random_factor)*board_width 

class Cloud():
    def __init__(self):
        self.name = "Cloud"
        self.xPos = choice([0.2,0.4,0.6])*board_width #position of upper left corner
        self.yPos = choice([0.2,0.4,0.6])*board_height #position of upper left corner
        self.width = 200
        self.height = 100

#==============================================================

class Pong():
    def __init__(self):
        self.Initialize()

        self.event_dictionary = {self.Miss_Ball : self.Miss_Response,
                                self.Hit_Paddle : self.Paddle_Response,
                                self.Hit_Portal : self.Portal_Response,
                                self.Hit_Wall : lambda: self.ball.Plane_Response(self.ball,"horizontal", self.player1.width, self.player2.width),
                                lambda: self.Hit_Sphere(self.monster) : self.Monster_Response,
                                lambda: self.Hit_Sphere(self.bird1) : lambda: self.Bird_Response(self.bird1),
                                lambda: self.Hit_Sphere(self.bird2) : lambda: self.Bird_Response(self.bird2),
                                lambda: self.Hit_Sphere(self.bird3) : lambda: self.Bird_Response(self.bird3),
                                self.Hit_Cloud : self.Cloud_Response,
                                self.Monster_Hit_Wall : lambda: self.monster.Plane_Response(self.monster, self.plane, self.player1.width, self.player2.width),
                                lambda: self.Event_Timings("Monster Move", 1, 5) : self.monster.Random_Velocity_Angle,
                                lambda: self.Event_Timings("Cloud Move", 1, 10) : self.Move_Cloud,
                                lambda: self.Event_Timings("Portals Move", 1, 8) : self.Move_Portal,
                                lambda: self.Event_Timings("Gravity Change", 1, 3) : self.Gravity_Response,
                                lambda: self.Event_Timings("Round Paddle", 1, 10) : lambda: self.Paddle_Shape_Change(1),
                                lambda: self.Event_Timings("Regular Paddle Shape", 0, 10, 5) : lambda: self.Paddle_Shape_Change(0),
                                lambda: self.Hit_Round_Paddle(self.player1) : lambda: self.Round_Paddle_Response(self.player1),
                                lambda: self.Hit_Round_Paddle(self.player2) : lambda: self.Round_Paddle_Response(self.player2),
                                lambda: self.Event_Timings("Reverse Paddle", 1, 8) : lambda: self.Paddle_Reverse_Direction(1),
                                lambda: self.Event_Timings("Regular Paddle Direction", 0, 8, 4) : lambda: self.Paddle_Reverse_Direction(0)}
    
    # Initialize mathematical objects, and set constants 
    def Initialize(self): 
        self.Time = 0
        self.Time_ms = 0
        self.EventStartTime = 0 # time that a particular event happened
        self.round_paddle = 0 # 0 if normal, 1 if reverses
        self.scene = "day"
        self.xGravity = 0
        self.yGravity = 0
        self.Gravity_Angle = 0
        self.plane = "horizontal"
        self.player1_score = 0
        self.player2_score = 0
        self.hits = 0 # number of consecutive hits by players
        self.reset = False # checks if program has been reset
        #==========================================================
        # Create ball, paddle, monster, portal, cloud objects from their classes
        self.ball = Ball(self.xGravity, self.yGravity)
        self.player1 = Paddle("Player1", 0,'w', 's')
        self.player2 = Paddle("Player2",board_width, 'Up', 'Down')
        self.monster = Monster("Monster", 70, "yellow", velocity = 200)
        self.portaltop = Portal("PortalTop", 0)
        self.portalbottom = Portal("PortalBottom", board_height)
        self.cloud = Cloud()
        self.Initialize_Birds()

    def Initialize_Birds(self):
        self.bird1 = Monster("Bird1", 44, 'firebrick2', velocity = 0)
        self.bird2 = Monster("Bird2", 44, 'black', velocity = 0)
        self.bird3 = Monster("Bird3", 44, 'gold', velocity = 0)
 
    #----------------------------------------------------------------
    # EVents and Responses

    def Hit_Wall(self): #detect hitting walls 
        bottom_wall = (self.ball.yPos + self.ball.radius) >= board_height
        top_wall = (self.ball.yPos - self.ball.radius) <= 0
        bottom_portal = (self.portalbottom.xPos - self.portalbottom.width/2) <= self.ball.xPos <= (self.portalbottom.xPos + self.portalbottom.width/2)
        top_portal = (self.portaltop.xPos - self.portaltop.width/2) <= self.ball.xPos <= (self.portaltop.xPos + self.portaltop.width/2)
        return (bottom_wall and not bottom_portal) or (top_wall and not top_portal)

    def Miss_Ball(self): # detect getting goal
        right_goal = (self.ball.xPos + self.ball.radius) >= (board_width)# - self.player2.width)
        left_goal = (self.ball.xPos - self.ball.radius) <= 0 #self.player1.width
        right_paddle = (self.player2.yPos - self.player2.length/2) <= self.ball.yPos <= (self.player2.yPos + self.player2.length/2)
        left_paddle = (self.player1.yPos - self.player1.length/2) <= self.ball.yPos <= (self.player1.yPos + self.player1.length/2)
        return (right_goal and not right_paddle) or (left_goal and not left_paddle)

    def Miss_Response(self):#/Redraw Ball, Paddles, etc./Update Score
        #checks to see who gains point 
        if self.ball.xPos < board_width/2: # ball on left side of board
            self.player2_score += 1
            self.hits = 0
        if self.ball.xPos > board_width/2: # ball on right side of board
            self.player1_score += 1
            self.hits = 0 

        # resets mathematical ball, paddles, and birds  
        self.ball.Initial_Conditions()
        self.player1.Initialize_Paddle('w','s')
        self.player2.Initialize_Paddle('Up','Down') 
        self.Initialize_Birds()
        # resets paddle direction and gravity
        self.round_paddle = 0
        self.xGravity = 0
        self.yGravity = 0

        view.Miss_Response()

    def Hit_Paddle(self): #detect hitting paddle
        right_goal = (self.ball.xPos + self.ball.radius) >= (board_width - self.player2.width)
        left_goal = (self.ball.xPos - self.ball.radius) <= self.player1.width
        right_paddle = (self.player2.yPos - self.player2.length/2) <= self.ball.yPos <= (self.player2.yPos + self.player2.length/2)
        left_paddle = (self.player1.yPos - self.player1.length/2) <= self.ball.yPos <= (self.player1.yPos + self.player1.length/2)
        return (right_goal and right_paddle) or (left_goal and left_paddle)

    def Paddle_Response(self):#/Redraw Ball #reflect and update hits
        self.ball.Plane_Response(self.ball, "vertical", self.player1.width, self.player2.width)
        # updates number of consecutive hits to increase game difficulty after every 5 hits
        self.hits += 1
        if self.hits % 5 ==0: # that is, 5, 10, 15...
            self.ball.Increase_Speed() 
        
        view.Delete_Object([self.ball])
        view.Draw_Ball()

    def Hit_Portal(self):
        bottom_wall = (self.ball.yPos + self.ball.radius) >= board_height
        top_wall = (self.ball.yPos - self.ball.radius) <= 0
        bottom_portal = (self.portalbottom.xPos - self.portalbottom.width/2) <= self.ball.xPos <= (self.portalbottom.xPos + self.portalbottom.width/2)
        top_portal = (self.portaltop.xPos - self.portaltop.width/2) <= self.ball.xPos <= (self.portaltop.xPos + self.portaltop.width/2)
        return (bottom_wall and bottom_portal) or (top_wall and top_portal)

    def Portal_Response(self):
        self.ball.Portal_Response(self.portaltop.xPos, self.portalbottom.xPos)
        view.Delete_Object([self.ball])
        view.Draw_Ball()

    def Hit_Sphere(self, sphere): #detect ball hitting sphere (monster, bird, paddle)
        relative_distance = sqrt((self.ball.xPos - sphere.xPos)**2 + (self.ball.yPos - sphere.yPos)**2)
        objects_distance = (sphere.radius + self.ball.radius) >= relative_distance
        return objects_distance

    def Monster_Response(self):
        self.ball.Sphere_Response(self.monster)
        #The following lines prevent the monster from catching up
        next_relative_distance = sqrt((self.ball.xPos + self.ball.xVelocity*time_interval - self.monster.xPos - self.monster.xVelocity*time_interval)**2 + (self.ball.yPos + self.ball.yVelocity*time_interval- self.monster.yPos - self.monster.yVelocity*time_interval)**2)
        monster_ball_distance = (self.monster.radius + self.ball.radius) >= next_relative_distance
        if monster_ball_distance:
            self.ball.xPos += self.monster.xVelocity*time_interval
            self.ball.yPos += self.monster.yVelocity*time_interval
        self.ball.xVelocity *= 1.2
        self.ball.yVelocity *= 1.2
        #reverse day/night
        if self.scene == "day":
            self.scene = "night"
        elif self.scene == "night":
            self.scene = "day"   

        view.Delete_Object([self.monster]) 
        view.Draw_Monster(scene = self.scene)
        view.Create_Background(scene = self.scene)
        
    def Bird_Response(self, bird): #reflects, deletes bird
        self.ball.Sphere_Response(bird)
        #puts bird off screen
        bird.xPos, bird.yPos = -25, -25
        view.Delete_Object([bird])

    def Monster_Hit_Wall(self): #monster hitting any wall
        bottom_wall = (self.monster.yPos + self.monster.radius) >= board_height
        top_wall = (self.monster.yPos - self.monster.radius) <= 0
        right_wall = (self.monster.xPos + self.monster.radius) >= (board_width - self.player2.width)
        left_wall = (self.monster.xPos - self.monster.radius) <= self.player1.width
        if (bottom_wall or top_wall): self.plane = "horizontal" 
        if (right_wall or left_wall): self.plane = "vertical" 
        return (bottom_wall or top_wall or right_wall or left_wall)
    
    def Hit_Cloud(self):# detects if hit cloud
        inx = self.cloud.xPos <= self.ball.xPos <= (self.cloud.xPos + self.cloud.width)
        iny = self.cloud.yPos <= self.ball.yPos <= (self.cloud.yPos + self.cloud.height)
        return (inx and iny)

    def Cloud_Response(self): # move ball to random edge of cloud and give new random velocity
        side = randint(1,4) # sides 1,2,3,4
        if side == 1:
            self.ball.yPos = self.cloud.yPos
            self.ball.xPos = self.cloud.xPos + self.cloud.width/2 
            self.ball.Random_Velocity_Angle([135,225]) 
        if side == 3: 
            self.ball.yPos = self.cloud.yPos + self.cloud.height
            self.ball.xPos = self.cloud.xPos + self.cloud.width/2 
            self.ball.Random_Velocity_Angle([315,45]) 
        if side == 2:
            self.ball.xPos = self.cloud.xPos
            self.ball.yPos = self.cloud.yPos + self.cloud.height/2 
            self.ball.Random_Velocity_Angle([225,315])
        if side == 3:
            self.ball.xPos = self.cloud.xPos + self.cloud.width
            self.ball.yPos = self.cloud.yPos + self.cloud.height/2 
            self.ball.Random_Velocity_Angle([45,135])
        view.Delete_Object([self.ball])
        view.Draw_Ball()

    def Event_Timings(self, eventname, state, frequency, duration = None):
        if state == 1: # On, that is, reverse speed every 9 sec for duration of 3 sec 
            if (self.Time_ms % (frequency * 1000)  == 0) and (self.Time_ms > 0):
                self.EventStartTime = self.Time_ms
                return True 
        if state == 0: # Off, returns speed to original after 3s of reversal
            return (self.Time_ms == self.EventStartTime+(duration * 1000)) and self.Time_ms>0
    
    def Gravity_Response(self): #changes gravity angle
        gg = 700
        self.Gravity_Angle = choice(angle_list) * pi/180
        self.xGravity = gg*sin(self.Gravity_Angle)
        self.yGravity = gg*cos(self.Gravity_Angle)
        # name = "Gravity Arrow"
        # view.Delete_Object(name)
        # view.Draw_Arrow(name=name)

    def Move_Portal(self): #Move_Randomly/Redraw Portals
        self.portaltop.Move_Randomly()
        self.portalbottom.Move_Randomly()
        view.Delete_Object([self.portalbottom, self.portaltop])
        view.Draw_Portals()

    def Move_Cloud(self): #move cloud randomly
        self.cloud.xPos = choice([0.2,0.4,.6])*board_width
        self.cloud.yPos = choice([0.2,0.4,.6])*board_height
        view.Delete_Object([self.cloud])
        view.Draw_Cloud()

    def Paddle_Shape_Change(self, state):
        if state == 1: #round paddle
            self.round_paddle = 1
            view.Draw_Round_Paddle()
        if state == 0: #regular paddle
            self.round_paddle = 0
            view.Delete_Object([self.player1, self.player2])
            view.Draw_Paddles()

    def Hit_Round_Paddle(self, sphere):
        # True only if paddle is reversed AND there is collision with spherical paddle
        return (self.round_paddle == 1) and self.Hit_Sphere(sphere)

    def Round_Paddle_Response(self,paddle):
        self.ball.Sphere_Response(paddle)
        view.Delete_Object([self.ball])
        view.Draw_Ball()

    def Paddle_Reverse_Direction(self, state): #change up/down
        # statement = 'Paddles reversed!' 
        if state == 1: # reverse        
            self.player1.Paddle_Direction(up = 's', down = 'w')
            self.player2.Paddle_Direction(up = 'Down', down = 'Up')
            # view.Draw_Statements(statement, board_height*0.9, color="Slate Blue", name = statement)          
        if state == 0: # normal
            self.player1.Paddle_Direction(up = 'w', down = 's')
            self.player2.Paddle_Direction(up = 'Up', down = 'Down')
            # view.Delete_Object(statement)

    # EVent handling
    def Event_Handler(self):
        for event, response in self.event_dictionary.items():
            if event(): response()
 
    # Restart Game
    def Restart(self):
        # restarts game by setting reset state to True, then reinitializing objects and board
        self.reset = True
        self.Initialize()
        view.Draw_Board()

    # Updating Model
    def Update(self, buttons):

        # checks if program is in a state of reset, if so restarts program
        if self.reset == True:
            self.Restart()
            self.reset = False

        self.ball.Update_Position(self.xGravity, self.yGravity) 
        self.monster.Update_Position(0,0)

        self.player1.Move(buttons)
        self.player2.Move(buttons)

        self.Event_Handler() 

        # updating incremental position (for incrementally updating view in time loop)
        self.ball.dx = self.ball.xVelocity * time_interval
        self.ball.dy = self.ball.yVelocity * time_interval
        self.monster.dx = self.monster.xVelocity * time_interval
        self.monster.dy = self.monster.yVelocity * time_interval
        # Not sure how to do this for paddles: the following doesn't work obviously. moved ball and monster dx dy into update position function
        # self.player1.dy = self.player1.velocity * time_interval
        # self.player2.dy = self.player1.velocity * time_interval
      
        self.Update_Time() # Update Time, since Update function will be called in a loop

    def Update_Time(self):
        self.tt = int(time_interval * 1000) # ms (milli seconds)
        self.Time += time_interval # current time in s
        self.Time_ms += self.tt # current time in ms


'''View'''
import Tkinter as tk
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg

class GUI(tk.Frame):
    def __init__(self, model, master = None):
        tk.Frame.__init__(self, master)
        self.master = master
        self.master.title("Play Pong!")
        self.grid()
        self.pack(side="top", fill="both", expand=True)
        self.grid_rowconfigure(0, weight=1)
        self.grid_columnconfigure(0, weight=1)

        self.Equate_Parameters() # To be able to use model math

        self.Create_Widgets()
        self.Draw_Board()
        self.pack()

    ''' Following function allows us to use information from model
        Do NOT change model parameters
        Only allowed to use them
        Example: use ball size, position, color, velocity to create and move tkinter ball'''
    def Equate_Parameters(self):
        self.ball = model.ball
        self.player1 = model.player1
        self.player2 = model.player2
        self.monster = model.monster
        self.cloud = model.cloud
        self.bird1 = model.bird1
        self.bird2 = model.bird2
        self.bird3 = model.bird3
        self.portaltop = model.portaltop
        self.portalbottom = model.portalbottom
        self.Gravity_Angle = model.Gravity_Angle
        self.scene = model.scene
        self.Time = model.Time
        self.Time_ms = model.Time_ms
        self.player1_score = model.player1_score
        self.player2_score = model.player2_score

    def Draw_Board(self):
        # create canvas
        self.Equate_Parameters()
        self.w = tk.Canvas(self, width=board_width, height=board_height, bg="black", relief="raised")
        self.w.grid(column=0, row=1, columnspan=2)
        # creates physical ball, paddles, portal, moster(sun), birds, gravity arrow, as well as the score keeper (text)
        self.Draw_Ball()
        self.Draw_Paddles()
        self.Draw_Monster()
        self.Draw_Portals()
        self.Draw_Cloud()
        # self.Draw_Arrow('Gravity Arrow')
        self.Draw_Birds()        
        # creates a background 
        self.Create_Background(self.scene) 
        # delete arrow and paddle reversal statement initially, will be re-created when event called
        # self.w.delete("Gravity Arrow")
        # update clock and score
        self.Update_Score()
        self.Update_Clock()
        
    def Create_Widgets(self): #quit/restart
       # buttons for restart and quit
        self.restart = tk.Button(self, text="Restart", command=model.Restart).grid(column=0,row=0, sticky = 'W')
        self.quit = tk.Button(self, text="Quit", command=self.quit).grid(column=1,row=0)

    def Create_Background(self, scene):
        self.w.create_line( board_width/2, board_height, board_width/2, 0, fill="white", dash=(4,4),  tag="board_divider")
        self.w.lower("board_divider")
        # creates a background 
        if scene == "day":
            self.sky = tk.PhotoImage(file= "daysky4.gif") 
        elif scene == "night":
            self.sky = tk.PhotoImage(file="nightsky3.gif")
        background = self.w.create_image(board_width/2, board_height/2, image=self.sky)
        # self.w.lower("board_divider")
        self.w.lower(background)

    def Draw_Ball(self):
        # physical ball. Positions defined by upper left and lower right corners of a bounding box
        self.w.create_oval(self.ball.xPos-self.ball.radius, 
                         self.ball.yPos+self.ball.radius, 
                         self.ball.xPos+self.ball.radius, 
                         self.ball.yPos-self.ball.radius, 
                         fill = self.ball.color, tag = self.ball.name)      
  
    def Draw_Paddles(self):
        # physical paddles. Positions defined by upper left and lower right corners of rectangle. 
        self.w.create_rectangle( 0, 
                              ( self.player1.yPos - self.player1.length/2 ), 
                                self.player1.width, 
                              ( self.player1.yPos + self.player1.length/2 ), 
                                fill = self.player1.color, tag = self.player1.name)
        self.w.create_rectangle( (board_width - self.player2.width), 
                                (self.player2.yPos - self.player1.length/2), 
                                 board_width, 
                                (self.player2.yPos + self.player2.length/2), 
                                 fill = self.player2.color, tag = self.player2.name)

    def Draw_Round_Paddle(self):
        self.w.delete("Player1", "Player2") # delete old paddles and create new shape

        self.w.create_oval( -self.player1.radius, 
                        ( self.player1.yPos - self.player1.radius ), 
                          self.player1.radius, 
                        ( self.player1.yPos + self.player1.radius), 
                          fill = self.player1.color, tag = self.player1.name)
        self.w.create_oval( (board_width - self.player2.radius), 
                           (self.player2.yPos - self.player1.radius), 
                            board_width + self.player2.radius, 
                           (self.player2.yPos + self.player2.radius), 
                            fill = self.player2.color, tag = self.player2.name)

    def Draw_Monster(self, scene = "day"): 
       # basic guides
        left = self.monster.xPos - self.monster.radius
        right = self.monster.xPos + self.monster.radius
        width = right - left
        top = self.monster.yPos - self.monster.radius
        bottom = self.monster.yPos + self.monster.radius
        height = bottom - top 

        if scene=="day":
            self.w.create_oval(left,top,right,bottom, fill="gold", tag = self.monster.name)

            eyecolor ='black'
            thickness=2
            # quarter guides
            centerleft = (self.monster.xPos + left) / 2
            centerright = (self.monster.xPos + right) / 2
            middletop = (self.monster.yPos + top) / 2
            middlebottom = (self.monster.yPos + bottom) / 2
            # eye guides
            eyeradius = ((width+height)/2) / 15
            eye1center = (self.monster.xPos + centerleft) / 2
            eye2center = (self.monster.xPos + centerright) / 2
            eyemiddle = middletop+6
            # smile guides 
            smileheight = height / 8
            smiletop = self.monster.yPos+5
            smileleft = centerleft
            smileright = centerright

            #draw rays
            self.w.create_line(right, self.monster.yPos, right + 20, self.monster.yPos,width=thickness,fill=self.monster.color, tag = self.monster.name)
            self.w.create_line(left - 20, self.monster.yPos, left, self.monster.yPos,width=thickness,fill=self.monster.color, tag = self.monster.name)
            self.w.create_line(self.monster.xPos, top, self.monster.xPos, top - 20,width=thickness,fill=self.monster.color, tag = self.monster.name)
            self.w.create_line(self.monster.xPos, bottom, self.monster.xPos, bottom + 20,width=thickness,fill=self.monster.color, tag = self.monster.name)
            self.w.create_line(self.monster.xPos + self.monster.radius/sqrt(2), self.monster.yPos + self.monster.radius/sqrt(2), self.monster.xPos + self.monster.radius/sqrt(2) + 20, self.monster.yPos + self.monster.radius/sqrt(2) + 20,width=thickness,fill=self.monster.color, tag = self.monster.name)
            self.w.create_line(self.monster.xPos + self.monster.radius/sqrt(2), self.monster.yPos - self.monster.radius/sqrt(2), self.monster.xPos + self.monster.radius/sqrt(2) + 20, self.monster.yPos - self.monster.radius/sqrt(2) - 20,width=thickness,fill=self.monster.color, tag = self.monster.name)
            self.w.create_line(self.monster.xPos - self.monster.radius/sqrt(2), self.monster.yPos + self.monster.radius/sqrt(2), self.monster.xPos - self.monster.radius/sqrt(2) - 20, self.monster.yPos + self.monster.radius/sqrt(2) + 20,width=thickness,fill=self.monster.color, tag = self.monster.name)
            self.w.create_line(self.monster.xPos - self.monster.radius/sqrt(2), self.monster.yPos - self.monster.radius/sqrt(2), self.monster.xPos - self.monster.radius/sqrt(2) - 20, self.monster.yPos - self.monster.radius/sqrt(2) - 20,width=thickness,fill=self.monster.color, tag = self.monster.name)

            # draw outer circle
            self.w.create_oval(left,top, right,bottom,
                               fill=self.monster.color, width=thickness, tag = self.monster.name)
            # draw eyes
            self.w.create_oval(eye1center-eyeradius,eyemiddle-eyeradius,
                               eye1center+eyeradius,eyemiddle+eyeradius,
                               fill=eyecolor, width=thickness/2, tag = self.monster.name)
            self.w.create_oval(eye2center-eyeradius,eyemiddle-eyeradius,
                               eye2center+eyeradius,eyemiddle+eyeradius,
                               fill=eyecolor, width=thickness/2, tag = self.monster.name)
            # draw smile
            self.w.create_arc(smileleft,smiletop-smileheight,
                              smileright,smiletop+2*smileheight,
                              start=0, extent=-180, width=thickness*2, style=tk.ARC, tag = self.monster.name)
        
        if scene=="night":
           # halo = self.radialGradientCircle(left-15,top-15, right+15, bottom+15, 72,61,139,  255,255,240   , self.monster.name) #    238,238,224)
            self.w.create_oval(left,top, right,bottom, fill="ivory", tag = self.monster.name)
            self.moon1 = tk.PhotoImage(file="moon1.gif").subsample(2,2)
            self.w.create_image(self.monster.xPos-1, self.monster.yPos-1, image=self.moon1, tag=self.monster.name)
        
        self.w.lower(self.monster.name)

    def Draw_Cloud(self):
        color = "Lavender" # "alice blue"
        self.w.create_rectangle(self.cloud.xPos, self.cloud.yPos, self.cloud.xPos + self.cloud.width, self.cloud.yPos + self.cloud.height, fill = color, outline = color, tag = self.cloud.name)

        self.w.create_oval(self.cloud.xPos, self.cloud.yPos-10, self.cloud.xPos + self.cloud.width/4, self.cloud.yPos + 10, fill = color, outline = color, tag = self.cloud.name)
        self.w.create_oval(self.cloud.xPos + self.cloud.width/4, self.cloud.yPos-10, self.cloud.xPos + self.cloud.width/2, self.cloud.yPos + 10, fill = color, outline = color, tag = self.cloud.name)
        self.w.create_oval(self.cloud.xPos + self.cloud.width/2, self.cloud.yPos-10, self.cloud.xPos+ 3*self.cloud.width/4, self.cloud.yPos + 10, fill = color, outline = color, tag = self.cloud.name)
        self.w.create_oval(self.cloud.xPos + 3*self.cloud.width/4, self.cloud.yPos-10, self.cloud.xPos + self.cloud.width, self.cloud.yPos + 10, fill = color, outline = color, tag = self.cloud.name)
        self.w.create_oval(self.cloud.xPos-10, self.cloud.yPos, self.cloud.xPos + 10, self.cloud.yPos + self.cloud.height/2, fill = color, outline = color, tag = self.cloud.name)
        self.w.create_oval(self.cloud.xPos-10, self.cloud.yPos+self.cloud.height/2, self.cloud.xPos+ 10, self.cloud.yPos + self.cloud.height, fill = color, outline = color, tag = self.cloud.name)

        self.w.create_oval(self.cloud.xPos, self.cloud.yPos-10+self.cloud.height, self.cloud.xPos + self.cloud.width/4, self.cloud.yPos + 10+self.cloud.height, fill = color, outline = color, tag = self.cloud.name)
        self.w.create_oval(self.cloud.xPos + self.cloud.width/4, self.cloud.yPos-10+self.cloud.height, self.cloud.xPos + self.cloud.width/2, self.cloud.yPos + 10+self.cloud.height, fill = color, outline = color, tag = self.cloud.name)
        self.w.create_oval(self.cloud.xPos+ self.cloud.width/2, self.cloud.yPos-10+self.cloud.height, self.cloud.xPos + 3*self.cloud.width/4, self.cloud.yPos + 10+self.cloud.height, fill = color, outline = color, tag = self.cloud.name)
        self.w.create_oval(self.cloud.xPos + 3*self.cloud.width/4, self.cloud.yPos-10+self.cloud.height, self.cloud.xPos + self.cloud.width, self.cloud.yPos + 10+self.cloud.height, fill = color, outline = color, tag = self.cloud.name)
        self.w.create_oval(self.cloud.xPos-10+self.cloud.width, self.cloud.yPos, self.cloud.xPos + 10+self.cloud.width, self.cloud.yPos + self.cloud.height/2, fill = color, outline = color, tag = self.cloud.name)
        self.w.create_oval(self.cloud.xPos-10+self.cloud.width, self.cloud.yPos+self.cloud.height/2, self.cloud.xPos + 10+self.cloud.width, self.cloud.yPos + self.cloud.height, fill = color, outline = color, tag = self.cloud.name)

        self.photo = tk.PhotoImage(file = 'angry_face2.gif')
        self.w.create_image( self.cloud.xPos+self.cloud.width/2, self.cloud.yPos+self.cloud.height/2, image=self.photo, tag = self.cloud.name)

    def Draw_Bird(self, bird, name, pic): # draws one bird
        self.bird1 = model.bird1
        self.bird2 = model.bird2
        self.bird3 = model.bird3
        self.w.create_oval( bird.xPos-bird.radius, 
                            bird.yPos+bird.radius, 
                            bird.xPos+bird.radius, 
                            bird.yPos-bird.radius, 
                            fill = bird.color, outline = bird.color ,
                            tag = name)
        pic = self.w.create_image(bird.xPos, bird.yPos, image = pic, tag = name)
        self.w.scale( pic, bird.xPos, bird.yPos, 2, 2)

    def Draw_Birds(self): # uses above function to draw 3 birds
        self.birdpic1 = tk.PhotoImage(file = 'angry_bird1.gif').subsample(2,2)
        self.birdpic2 = tk.PhotoImage(file = 'angry_bird2.gif').subsample(2,2)
        self.birdpic3 = tk.PhotoImage(file = 'angry_bird3.gif').subsample(2,2)

        self.Draw_Bird(self.bird1, self.bird1.name, self.birdpic1)
        self.Draw_Bird(self.bird2, self.bird2.name, self.birdpic2)
        self.Draw_Bird(self.bird3, self.bird3.name, self.birdpic3)
    
    def Draw_Portals(self):
        # create TKinter portals as thick lines
        self.w.create_line( (self.portaltop.xPos - self.portaltop.width/2), 0,
                           (self.portaltop.xPos + self.portaltop.width/2), 0, 
                           fill = self.portaltop.color, width = 20, tag = self.portaltop.name)
        self.w.create_line( (self.portalbottom.xPos - self.portalbottom.width/2), board_height, 
                           (self.portalbottom.xPos + self.portalbottom.width/2), board_height, 
                           fill = self.portalbottom.color, width = 10, tag = self.portalbottom.name)

    def Draw_Arrow(self, name):
        # display an arrow to show direction of gravity field
        self.w.create_line( board_width/2,
                            board_height/2,
                            board_width/2 + 70*sin(self.Gravity_Angle),
                            board_height/2 + 70*cos(self.Gravity_Angle),
                            fill = "red", width=2, arrow = "last", tag = name)  

    def Draw_Statements(self, statement, placement, color, name):
        return self.w.create_text( board_width/2, placement, text=statement, fill=color, font="Arial 14", tag = name)

    def Update_Score(self):
        self.Equate_Parameters()
        # score displayer
        self.p1score = self.w.create_text( board_width/2 - 150, 50, text=self.player1_score, fill="blue", font="Arial 30")
        self.p2score = self.w.create_text( board_width/2 + 150, 50, text=self.player2_score, fill="blue", font="Arial 30")    
        # print "update score" , self.player1_score, self.player2_score
    
    def Update_Clock(self):
        self.Equate_Parameters()
        # displays time
        seconds = self.Time%60 - (self.Time%60)%1
        minutes = (self.Time-self.Time%60)/60

        time_string = "%02d:%02d" % (minutes, seconds)
        self.time_display = self.w.create_text(board_width/2, 30, text=time_string, fill="white", font="Arial 20") 
      
    # Made one function so it could be directly called in Model, instead of executing several view functions
    def Miss_Response(self):
        self.Equate_Parameters()
        # resets physical ball and paddles
        # self.Delete_Object([self.ball, self.player1, self.player2, self.p1score, self.p2score, self.bird1, self.bird2, self.bird3])
        self.w.delete(self.ball.name, self.player1.name, self.player2.name, self.p1score, self.p2score, self.bird1.name, self.bird2.name, self.bird3.name) #(, "Gravity_Arrow", "paddle_rev_statement")
        self.Draw_Ball()
        self.Draw_Paddles()
        self.Draw_Birds()

        # updates score
        self.Update_Score()

    def Delete_Object(self, object_list): #need for redrawing and birds
        for item in object_list:
            # if type(item) == str:
            #     print item, type(item)
            #     self.w.delete(item)
            # if type(item) == object:
            #     print item , type(item)
            self.w.delete(item.name)
 
    # Update View 
    def Update(self, tt, Time_Loop): # one recursive function to be used in time loop to move all physical things
        self.Equate_Parameters()

        self.w.delete(view.time_display)
        self.Update_Clock()

        dy_player1 = self.player1.yPos - self.w.coords(self.player1.name)[1] - self.player1.length/2
        dy_player2 = self.player2.yPos - self.w.coords(self.player2.name)[1] - self.player1.length/2

        self.w.move(self.player1.name, 0, dy_player1)     
        self.w.move(self.player2.name, 0, dy_player2)     
        self.w.move(self.ball.name, self.ball.dx, self.ball.dy)
        self.w.move(self.monster.name, self.monster.dx, self.monster.dy)

        self.w.after(tt, Time_Loop) # tkinter's recursice function


'''Controller'''
class Keyboard(tk.Frame):
    def __init__(self, master = None):
        tk.Frame.__init__(self, master)
        self.master = master
        self.Button_Pressed()
    def Button_Pressed(self): 
        self.buttons = set()
        self.bind_all('<KeyPress>', lambda event: self.buttons.add(event.keysym))
        self.bind_all('<KeyRelease>', lambda event: self.buttons.discard(event.keysym))
        
    
'''Main file'''
# Initialize Model, View, and Contoller
root = tk.Tk()

controller = Keyboard(master = root)
model = Pong()
view = GUI(model, master=root)

# Run recursive Time Loop that updates Model and View 
def Time_Loop():  
    model.Update(buttons = controller.buttons)   
    view.Update(model.tt, Time_Loop)

Time_Loop()
view.mainloop()
root.destroy() 